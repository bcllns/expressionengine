require 'sinatra'
require 'bundler'

Bundler.require
##########
#this is the API for creating
#managing accounts and locations
##########


####
# config
####
configure :development do
  MongoMapper.connection = Mongo::Connection.new('dogen.mongohq.com',10052, :pool_size => 5, :pool_timeout => 5)
  MongoMapper.database = "ee"
  MongoMapper.database.authenticate("dbadmin","mason12345")
end


####
# models
###
#require './models/admin.rb'

####
# controllers
###
require './controllers/admin.rb'

####
# routes
###
require './routes/admin.rb'
require './routes/public.rb'



##########for connection testing
class Connection
  def initialize()
    @base_uri = 'http://coupehome.dyndns.org:50700'
    
    @conn = Faraday.new(:url => @base_uri) do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
  end

  def get
    response = @conn.get do |req|
      req.headers['Authorization'] = 'Bearer ' + @token
      req.headers['Content-Type'] = 'application/json'
    end
    
    return response
  end

  def post(data, token)
    response = @conn.post do |req|
      #req.headers['Authorization'] = 'Bearer ' + token
      unless token.nil?
        req.headers['Authorization'] = 'Token ' + token
      end
      req.headers['Content-Type'] = 'application/json'
      req.body = data
    end
    return response
  end
end

get '/testconnection' do
   erb :'/testconnection'
end

post '/testconnection' do
  c = Connection.new()

  token = JWT.encode({"location_id" => 1234567890}, "rookjumpsqueen")
  
  data = "COMMAND " +  params[:command].downcase 

  data = {
    :command => params[:command],
    :request_id => BSON::ObjectId.new.to_s,
    :location_id => "1234567890"
  }

  
  response = c.post(data.to_json, token)
  p response.inspect

 "All done " #+ response.to_s

end


post '/callback/test' do

  p parmas.inspect
  p request[:env]

  status 200
end