######
# admin model - this is the AV/Automation installer/dealer, not the end-user
######

class Admin
  include MongoMapper::Document

  key :username, String #username/emails address
  key :password, String #pw
  key :status, Integer #0 = not active 1 = active 9 = delted/banned/suspended
  key :phone_number, String
  key :first_name, String
  key :last_name, String
  
  key :created_at
  key :updated_at

  timestamps!

end